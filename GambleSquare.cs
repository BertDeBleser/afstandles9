using System;

namespace afstandsles9
{
    class GambleSquare : ISquare
    {

        private string name;
        private JackpotSquare jackpotSquare;

        public GambleSquare(string n,JackpotSquare j) 
        {
            this.name = n;
            this.jackpotSquare = j;
        }
        
        public void ReactToVisit(Player p) 
        {
            Console.WriteLine($"{p.GetName()} komt terecht op {GetName()}");
            Console.WriteLine("Wat wil je doen? (1: 50 euro betalen; 2: double or nothing)");
            int ans = int.Parse(Console.ReadLine());
            if(ans == 1) {
                p.SetMoney(p.GetMoney() - 50);
                jackpotSquare.SetJackpot(jackpotSquare.GetJackpot() + 50);
            }
            else{
                int outcome = Utilities.RNG.Next(1,3);
                if(outcome == 1) {
                    p.SetMoney(p.GetMoney() - 100);
                    jackpotSquare.SetJackpot(jackpotSquare.GetJackpot() + 100);
                }
            }
            Console.WriteLine($"{p.GetName()} heeft nog €{p.GetMoney()}");
        }

        public void ReactToTraversal(Player p) 
        {
            //
            //
            //
        }

        public string GetName() 
        {
            return this.name;
        }
    }
}