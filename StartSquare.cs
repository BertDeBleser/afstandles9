﻿using System;

namespace afstandsles9
{
    class StartSquare : ISquare
    {
        
        public void ReactToVisit(Player p) {
            Console.WriteLine($"{p.GetName()} blijft staan op start.");
            Console.WriteLine("Je kan hier geen acties ondernemen.");
        }

        public void ReactToTraversal(Player p) {
            Console.WriteLine($"{p.GetName()} passeert start!");
            p.SetMoney(p.GetMoney() + 5000);
            Console.WriteLine($"{p.GetName()} ziet zijn rekening stijgen tot €{p.GetMoney()}");
        }

        public string GetName() {
            return "Start";
        }

    }
}
