﻿using System;

namespace afstandsles9
{
    class Player
    {
        private string name;
        private int pos;

        private int money;

        public Player(string name) 
        {
            this.name = name;
            this.SetPos(0);
            this.SetMoney(5000);
        }

        public string GetName() 
        {
            return this.name;
        }

        public int GetPos() 
        {
            return this.pos;
        }

        public void SetPos(int pos) 
        {
            this.pos = pos;
        }

        public void SetMoney(int money) 
        {
          this.money = money;
        }

        public int GetMoney() 
        {
            return money;
        }
    }
}
