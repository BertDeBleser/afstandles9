using System;

namespace afstandsles9
{
    class Board
    {
        private Player[] players;
        private ISquare[] squares;
        private int currentPlayerIndex;
        public Board(Player[] players)
        {
            this.players = players;
            string[] streetNames = {"Zemstbaan",
                                    "Acaciastraat",
                                    "Ankerstraat",
                                    "Groenstraat",
                                    "Terwenblok",
                                    "Welvaartstraat",
                                    "Nattehofstraat",
                                    "Sint-Jozefstraat",
                                    "Winketkaai",
                                    "Donkerlei",
                                    "Ridder Dessainlaan",
                                    "Bruul",
                                    "Lekkernijstraatje",
                                    "Willem Rosierstraat",
                                    "Kapelstraat ",
                                    "Tichelrij",
                                    "Beekstraat",
                                    "Straatje-zonder-einde",
                                    "Tivolilaan",
                                    "Botermarkt",
                                    "Maanstraat",
                                    "Graaf van Egmontstraat",
                                    "Olivetenvest"
                                    };
            ISquare[] squares = new ISquare[streetNames.Length + 2];
            squares[0] = new StartSquare();
            JackpotSquare jackpot = new JackpotSquare();
            squares[squares.Length - 1] = jackpot;
            for(int i = 0; i < streetNames.Length; i++) {
                squares[i+1] = new GambleSquare(streetNames[i],jackpot);
            }
            this.squares = squares;
        }
        public void PlayTurn()
        {
                Player currentPlayer = this.players[currentPlayerIndex];
                int roll1 = Utilities.RNG.Next(1,7);
                int roll2 = Utilities.RNG.Next(1,7);
                Console.WriteLine($"{currentPlayer.GetName()} rolde een {roll1 + roll2}.");
                int tempPos = currentPlayer.GetPos();
                for(int i = 1; i <= roll1 + roll2; i++) {
                    tempPos = (tempPos + 1) % squares.Length;
                    squares[tempPos].ReactToTraversal(currentPlayer);
                }
                currentPlayer.SetPos(tempPos);
                squares[currentPlayer.GetPos()].ReactToVisit(currentPlayer);                
                currentPlayerIndex = (currentPlayerIndex + 1) % players.Length;


        }
    }
}
