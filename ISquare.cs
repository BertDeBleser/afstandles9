﻿using System;

namespace afstandsles9 {
    interface ISquare 
    {
        void ReactToVisit(Player player);
        void ReactToTraversal(Player player);
        string GetName();// verkrijgt de naam 
    }
}