﻿using System;

namespace afstandsles9
{
    class JackpotSquare : ISquare
    {

        private int jackpot;

        public JackpotSquare() {
            jackpot = 0;
        }
        
        public void ReactToVisit(Player p) {
            Console.WriteLine($"{p.GetName()} wint de jackpot en ontvangt €{jackpot}!");
            p.SetMoney(p.GetMoney() + GetJackpot());
            SetJackpot(0);
            Console.WriteLine("Je kan hier geen acties ondernemen.");
        }

        public void ReactToTraversal(Player p) {
            // gebeurt op zich niets
        }

        public string GetName() {
            return "Jackpot";
        }

        public int GetJackpot() {
            return jackpot;
        }

        public void SetJackpot(int j) {
            if(j < 0) {
                throw new Exception("Kan geen negatieve jackpot hebben.");
            }
            else{
                jackpot = j;
            }
        }
    }
}
