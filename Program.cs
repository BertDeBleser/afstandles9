﻿using System;

namespace afstandsles9
{
    class Program
    {
            static void Main(string[] args)
            {
            Console.WriteLine("Welkom bij Monopoly!");
            Console.WriteLine("Hoe veel spelers zijn er?");
            int numberOfPlayers = int.Parse(Console.ReadLine());
            Player[] players = new Player[numberOfPlayers];
            for(int i = 1; i <= numberOfPlayers; i++) {
                Console.WriteLine($"Hoe heet speler {i}?");
                string playerName = Console.ReadLine();
                Player player = new Player(playerName); // start op pos 0
                players[i-1] = player;
            }
            Board board = new Board(players);
            while(true) {
                board.PlayTurn();
            }
        }    

    }
}